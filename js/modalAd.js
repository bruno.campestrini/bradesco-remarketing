window.onload=function(){
  $('#veragora_btn').on('mouseenter', function () {

      if ($(this).hasClass('step-1'))
          $(this).attr('class','step-2');
          
  });

  $('#veragora_btn').on('click', function () {

      if ($(this).hasClass('step-2')){
          $('#veragora_modal').addClass('active');
          $('.alt-modal-backdrop').addClass('active');
          $(this).attr('class','step-0');
      }
          
  });

  $('#veragora_modal .close').on('click', function () {
      $('#veragora_modal').removeClass('active');
      $('.alt-modal-backdrop').removeClass('active');
      $('#veragora_btn').attr('class','step-1');
  });
}